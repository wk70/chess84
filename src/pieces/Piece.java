package pieces;

public abstract class Piece{
	
	private String id;
	private boolean color;
	private boolean moved = false;
	private boolean captured = false;
	private String pieceType;
	private String prevPosition;
	private String currPosition;
	
	/**
	 * Abstract method that determines whether or not a given move is valid
	 * @param moveTo Desired position to move on the board
	 * @param piecelist grid of game pieces
	 * @return Returns boolean that indicates whether a move was valid or not
	 */
	public abstract boolean validMove(String moveTo,Piece[][] piecelist);
	
	/**
	 * Method that handles movement of a piece
	 * @param moveTo Desired position to move on the board
	 * @param piecelist grid of game pieces
	 * @return returns modified piecelist
	 */
	public abstract Piece[][] move(String moveTo, Piece[][] piecelist);

	/**
	 * 
	 * @param ID ID of gamepiece
	 */
	public void setID(String ID){
		this.id = ID;
	}
	/**
	 * 
	 * @param isWhite Controls color of piece. true = piece is white, false = piece is black
	 */	
	public void setColor(boolean isWhite){
		this.color = isWhite;
	}
	
	/**
	 * 
	 * @param didMove Boolean that controls whether or not piece has moved
	 */
	public void setMoved(boolean didMove){
		this.moved = didMove;
	}
	
	/**
	 * 
	 * @param cap Boolean that controls whether or not a piece has been captured
	 */
	public void setCaptured(boolean cap) {
		this.captured = cap;
	}
	
	/**
	 * 
	 * @param pieceType String that defines the piece type
	 * "B" = Bishop, "K" = King, "N" = Knight, "P" = Pawn, Q" = Queen, "R" = Rook
	 */
	public void setPieceType(String pieceType){
		this.pieceType = pieceType;
	}
	
	/**
	 * 
	 * @param prevPosition String that sets a piece's previous position
	 */
	public void setPrevPosition(String prevPosition){
		this.prevPosition = prevPosition;
	}
	
	/**
	 * 
	 * @param currPosition set the piece's current position
	 */
	public void setCurrPosition(String currPosition){
		this.currPosition = currPosition;
	}
	/**
	 * 
	 * @return ID of piece
	 */
	public String getID(){
		return id;
	}
	/**
	 * 
	 * @return color of piece, true = white, false = black
	 */
	public boolean getColor(){
		return color;
	}
	
	/**
	 * 
	 * @return boolean if piece was moved
	 */
	public boolean getMoved(){
		return moved;
	}
	
	/**
	 * 
	 * @return boolean if piece was captured
	 */
	public boolean getCaptured() {
		return captured;
	}
	
	/**
	 * 
	 * @return returns piece type, "B" = Bishop, "K" = King, "N" = Knight, "P" = Pawn, Q" = Queen, "R" = Rook
	 */
	public String getPieceType(){
		return pieceType;
	}
	
	/**
	 * 
	 * @return returns previous position of piece
	 */
	public String getPrevPosition(){
		return prevPosition;
	}
	
	/**
	 * 
	 * @return returns current position of piece
	 */
	public String getCurrPosition(){
		return currPosition;
	}
}