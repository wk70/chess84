package pieces;

import chess.Chess;

public class King extends Piece{
	
	
	private boolean inCheck = false;
	public King(String id, boolean c, String p) {
		setID(id);
		setColor(c);
		setPieceType("K");
		setPrevPosition(p);
		setCurrPosition(p);
	}
	
	
	/** 
	 * @param c
	 */
	public void setInCheck(boolean c) {
		this.inCheck = c;
	}
	
	
	/** 
	 * @return boolean
	 */
	public boolean getInCheck() {
		return inCheck;
	}
	
	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return Piece[][]
	 */
	public Piece[][] move(String newPosition, Piece[][] piecelist){
		return null;
	}

	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return boolean
	 */
	public boolean validMove(String newPosition, Piece[][] piecelist) {
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int oldX = currLocation[0];
		int oldY = currLocation[1];
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff = Math.abs(newX-oldX);
		int yDiff = Math.abs(newY-oldY);
		int d;
		if (this.getColor() == true) {
			d = 1;
		}
		else {
			d = 0;
		}
		
		if ((xDiff > 1) || (yDiff > 1)) {
			if ((xDiff == 2) && (this.getMoved() == false)){
				if ((newX-oldX) > 0) {
					if (piecelist[d][1].getMoved() == false) {
						if (this.getColor()) {
							for (int a = 0; a< 2;a++) {
								for (int b = 0; b < 16; b++) {
									if (piecelist[a][b].getCurrPosition().equals("f1") || piecelist[a][b].getCurrPosition().equals("g1")) {
										return false;
									}
								}
							}
							this.setCurrPosition("f1");
							if (checkChecker(piecelist,this.getColor())){
								return false;
							}
							this.setCurrPosition("g1");
							if (checkChecker(piecelist,this.getColor())) {
								return false;
							}
							return true;
						}
						else {
							for (int a = 0; a< 2;a++) {
								for (int b = 0; b < 16; b++) {
									if (piecelist[a][b].getCurrPosition().equals("f8") || piecelist[a][b].getCurrPosition().equals("g8")) {
										return false;
									}
								}
							}
							this.setCurrPosition("f8");
							if (checkChecker(piecelist,this.getColor())){
								return false;
							}
							this.setCurrPosition("g8");
							if (checkChecker(piecelist,this.getColor())) {
								return false;
							}
							this.setMoved(true);
							piecelist[d][1].setMoved(true);
							return true;
						}
					}		
					else {
						return false;
					}
				}
				else {
					if (piecelist[d][0].getMoved() == false) {
						if (this.getColor()) {
							for (int a = 0; a< 2;a++) {
								for (int b = 0; b < 16; b++) {
									if (piecelist[a][b].getCurrPosition().equals("c1") || piecelist[a][b].getCurrPosition().equals("d1")) {
										return false;
									}
								}
							}
							this.setCurrPosition("c1");
							if (checkChecker(piecelist,this.getColor())){
								return false;
							}
							this.setCurrPosition("d1");
							if (checkChecker(piecelist,this.getColor())) {
								return false;
							}
							this.setMoved(true);
							piecelist[d][0].setMoved(true);
							return true;
						}
						else {
							for (int a = 0; a< 2;a++) {
								for (int b = 0; b < 16; b++) {
									if (piecelist[a][b].getCurrPosition().equals("c8") || piecelist[a][b].getCurrPosition().equals("d8")) {
										return false;
									}
								}
							}
							this.setCurrPosition("c8");
							if (checkChecker(piecelist,this.getColor())){
								return false;
							}
							this.setCurrPosition("d8");
							if (checkChecker(piecelist,this.getColor())) {
								return false;
							}
							this.setMoved(true);
							piecelist[d][0].setMoved(true);
							return true;
						}
					}		
					else {
						return false;
					}
				}
			}
			else {
				return false;
			}
		}
		else {
			int capturing = 0;
			int captureX = 0;
			int captureY = 0;
			String tempPrevPosition = this.getPrevPosition();
			this.setPrevPosition(this.getCurrPosition());
			for (int a = 0; a< 2; a++) {
				for (int b = 0; b< 16; b++) {
					if(piecelist[a][b].getCurrPosition().equals(newPosition)) {
						if (piecelist[a][b].getColor() == this.getColor()) {
							return false;
						}
						else {
							capturing = 1;
							captureX = a;
							captureY = b;
							piecelist[a][b].setCaptured(true);
						}
					}
				}
			}
			this.setCurrPosition(newPosition);
			if(checkChecker(piecelist,this.getColor()) == true) {
				this.setCurrPosition(this.getPrevPosition());
				this.setPrevPosition(tempPrevPosition);
				if (capturing == 1) {
					piecelist[captureX][captureY].setCaptured(false);
					capturing = 0;
				}
				return false;
			}
			else {
				if (this.getMoved() == false) {
					this.setMoved(true);
				}
				return true;
			}
		}
	}
	
	
	/** 
	 * @param piecelist
	 * @param color
	 * @return boolean
	 */
	public static boolean checkChecker(Piece[][] piecelist, boolean color) {
		int d;
		if (color == true) {
			d = 1;
		}
		else {
			d = 0;
		}
		int[] k = Chess.convertMove(piecelist[d][7].getCurrPosition());
		String[][] currBoard = Chess.boardMaker(piecelist);
		
		for (int a = 0; a < 2; a++) {
			for (int b = 0; b < 16;b++) {
				int x = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
				int y = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
				String type = piecelist[a][b].getPieceType();
				boolean c = piecelist[a][b].getColor();
				int xDiff = Math.abs(k[1] - x);
				int yDiff = Math.abs(k[0] - y);
				int diagonalDifference = k[0]-k[1];
				int diagonalSum = k[0]+k[1];
				
				if ((c != color) && (piecelist[a][b].getCaptured() == false)) {
					if (type.equals("N")) {
						if ((xDiff == 2) && (yDiff == 1)||((xDiff == 1) && (yDiff == 2))) {
							System.out.println("p");
							return true;
						}
					}
					else if (type.equals("B")) {
						int piecesInWay = 0;
						if (xDiff == yDiff) {
							if (k[1] < x) {
								if (k[0] < y) {
									for (int l = k[1];l < x;l++) {
										for(int m = k[0];m<y;m++) {
											if((l-m)==diagonalDifference) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
								else if (k[0] > y) {
									for (int l = k[1];l<x;l++) {
										for (int m = k[0]; m<y;m++) {
											if ((l+m)==diagonalSum) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
							}
							else if (k[1] > x) {
								if(k[0] > y) {
									for (int l = k[1];l < x;l++) {
										for(int m = k[0];m<y;m++) {
											if((l-m)==diagonalDifference) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
								else if (k[0] < y) {
									for (int l = k[1];l < x;l++) {
										for(int m = k[0];m<y;m++) {
											if((l+m)==diagonalSum) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
							}
						}
						if (piecesInWay == 0) {
							System.out.println("p");
							return true;
						}
					}
					else if (type.equals("R")) {
						int piecesInWay = 0;
						if (k[1] == x) {
							if (k[0] < y) {
								for (int v = k[0];v<y;v++) {
									if (!currBoard[x][v].equals("  ") && !currBoard[x][v].equals("##")) {
										if (!currBoard[x][v].equals(piecelist[a][b].getID()) && !currBoard[x][v].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
							else if (k[0] > y) {
								for (int v = y;v<k[0];v++) {
									if (!currBoard[x][v].equals("  ") && !currBoard[x][v].equals("##")) {
										if (!currBoard[x][v].equals(piecelist[a][b].getID()) && !currBoard[x][v].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
						}
						else if (k[0] == y){
							if (k[1] < x) {
								for (int v = k[1];v<x;v++) {
									if (!currBoard[v][y].equals("  ") && !currBoard[v][y].equals("##")) {
										if (!currBoard[v][y].equals(piecelist[a][b].getID()) && !currBoard[v][y].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
							else if (k[1] > x) {
								for (int v = x;v<k[1];v++) {
									if (!currBoard[v][y].equals("  ") && !currBoard[v][y].equals("##")) {
										if (!currBoard[v][y].equals(piecelist[a][b].getID()) && !currBoard[v][y].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
						}
						if (piecesInWay == 0) {
							System.out.println("p4");
							return true;
						}
					}
					else if (type.equals("Q")) {
						int piecesInWay = 0;
						if (xDiff == yDiff) {
							if (k[1] < x) {
								if (k[0] < y) {
									for (int l = k[1];l < x;l++) {
										for(int m = k[0];m<y;m++) {
											if((l-m)==diagonalDifference) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
								else if (k[0] > y) {
									for (int l = k[1];l<x;l++) {
										for (int m = y; m<k[0];m++) {
											if ((l+m)==diagonalSum) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
							}
							else if (k[1] > x) {
								if(k[0] > y) {
									for (int l = x;l < k[1];l++) {
										for(int m = y;m<k[0];m++) {
											if((l-m)==diagonalDifference) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
								else if (k[0] < y) {
									for (int l = x;l < k[1];l++) {
										for(int m = k[0];m<y;m++) {
											if((l+m)==diagonalSum) {
												if (!(currBoard[l][m].equals("  ")) && (!currBoard[l][m].equals("##"))) {
													if (!currBoard[l][m].equals(piecelist[a][b].getID()) && !currBoard[l][m].equals(piecelist[d][7].getID())) {
														piecesInWay = piecesInWay+1;
													}
												}
											}
										}
									}
								}
							}
						}
						else if (k[1] == x) {
							if (k[0] < y) {
								for (int v = k[0];v<y;v++) {
									if (!currBoard[x][v].equals("  ") && !currBoard[x][v].equals("##")) {
										if (!currBoard[x][v].equals(piecelist[a][b].getID()) && !currBoard[x][v].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
							else if (k[0] > y) {
								for (int v = y;v<k[0];v++) {
									if (!currBoard[x][v].equals("  ") && !currBoard[x][v].equals("##")) {
										if (!currBoard[x][v].equals(piecelist[a][b].getID()) && !currBoard[x][v].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
						}
						else if (k[0] == y){
							if (k[1] < x) {
								for (int v = k[1];v<x;v++) {
									if (!currBoard[v][y].equals("  ") && !currBoard[v][y].equals("##")) {
										if (!currBoard[v][y].equals(piecelist[a][b].getID()) && !currBoard[v][y].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
							else if (k[1] > x) {
								for (int v = x;v<k[1];v++) {
									if (!currBoard[v][y].equals("  ") && !currBoard[v][y].equals("##")) {
										if (!currBoard[v][y].equals(piecelist[a][b].getID()) && !currBoard[v][y].equals(piecelist[d][7].getID())) {
											piecesInWay = piecesInWay+1;
										}
									}
								}
							}
						}
						if (piecesInWay != 0) {
							System.out.println("p3");
							return true;
						}
					}
					else if (type.equals("K")) {
						if((xDiff == 1 && yDiff == 1) || (xDiff == 0 && yDiff == 1) || (xDiff == 1 && yDiff == 0)) {
							System.out.println("p7");
							return true;
						}
					}
					else if (type.equals("p")) {
						if ((xDiff == 1) && ((k[1]-y) == 1) && (c == true)) {
							System.out.println("pp");
							return true;
						}
						else if ((xDiff == 1) && ((y-k[1])==1) && (c == true)) {
							System.out.println("ppp");
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}