package pieces;

import chess.Chess;
import java.lang.Math;

public class Queen extends Piece{
	
	public Queen(String id, boolean color, String location) {
		setID(id);
		setColor(color);
		setPieceType("Q");
		setPrevPosition(location);
		setCurrPosition(location);
	}
	
	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return boolean
	 */
	public boolean validMove(String newPosition, Piece[][] piecelist) {
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int oldX = currLocation[0];
		int oldY = currLocation[1];
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff = Math.abs(newX-oldX);
		int yDiff = Math.abs(newY-oldY);
		int diagonalDifference = oldX-oldY;
		int diagonalSum = oldX+oldY;
		
		if (xDiff == yDiff) {
			if (oldX < newX && oldY < newY) {
				for (int a = 0; a < 2;a++) {
					for (int b=0;b<16;b++) {
						int blockerX = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
						int blockerY = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
						if (blockerX-blockerY == diagonalDifference) {
							if ((blockerX > oldX && blockerX < newX) && (blockerY > oldY && blockerY < newY)) {
								return false;
							}
							else if (blockerX == newX && blockerY == newY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else {
									if (!piecelist[a][b].getPieceType().equals("K")) {
										String tempCurrPosition = this.getCurrPosition();
										this.setCurrPosition(newPosition);
										piecelist[a][b].setCaptured(true);
										if (King.checkChecker(piecelist, this.getColor())) {
											piecelist[a][b].setCaptured(false);
											this.setCurrPosition(tempCurrPosition);
											return false;
										}
										else {
											if (this.getMoved() == false) {
												this.setMoved(true);
											}
											return true;
										}
									}
									else {
										return false;
									}
								}
							}
						}
					}
				}
			}
			else if (oldX > newX && oldY > newY) {
				for (int a = 0; a < 2;a++) {
					for (int b=0;b<16;b++) {
						int blockerX = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
						int blockerY = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
						if (blockerX-blockerY == diagonalDifference) {
							if ((blockerX < oldX && blockerX > newX) && (blockerY < oldY && blockerY > newY)) {
								return false;
							}
							else if (blockerX == newX && blockerY == newY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else {
									if (!piecelist[a][b].getPieceType().equals("K")) {
										String tempCurrPosition = this.getCurrPosition();
										this.setCurrPosition(newPosition);
										piecelist[a][b].setCaptured(true);
										if (King.checkChecker(piecelist, this.getColor())) {
											piecelist[a][b].setCaptured(false);
											this.setCurrPosition(tempCurrPosition);
											return false;
										}
										else {
											if (this.getMoved() == false) {
												this.setMoved(true);
											}
											return true;
										}
									}
									else {
										return false;
									}
								}
							}
						}
					}
				}
			}
			else if (oldX > newX && oldY < newY) {
				for (int a = 0; a < 2;a++) {
					for (int b=0;b<16;b++) {
						int blockerX = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
						int blockerY = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
						if (blockerX-blockerY == diagonalSum) {
							if ((blockerX < oldX && blockerX > newX) && (blockerY > oldY && blockerY < newY)) {
								return false;
							}
							else if (blockerX == newX && blockerY == newY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else {
									if (!piecelist[a][b].getPieceType().equals("K")) {
										String tempCurrPosition = this.getCurrPosition();
										this.setCurrPosition(newPosition);
										piecelist[a][b].setCaptured(true);
										if (King.checkChecker(piecelist, this.getColor())) {
											piecelist[a][b].setCaptured(false);
											this.setCurrPosition(tempCurrPosition);
											return false;
										}
										else {
											if (this.getMoved() == false) {
												this.setMoved(true);
											}
											return true;
										}
									}
									else {
										return false;
									}
								}
							}
						}
					}
				}
			}
			else if (oldX < newX && oldY > newY) {
				for (int a = 0; a < 2;a++) {
					for (int b=0;b<16;b++) {
						int blockerX = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
						int blockerY = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
						if (blockerX+blockerY == diagonalSum) {
							if ((blockerX > oldX && blockerX < newX) && (blockerY < oldY && blockerY > newY)) {
								return false;
							}
							else if (blockerX == newX && blockerY == newY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else {
									if (!piecelist[a][b].getPieceType().equals("K")) {
										String tempCurrPosition = this.getCurrPosition();
										this.setCurrPosition(newPosition);
										piecelist[a][b].setCaptured(true);
										if (King.checkChecker(piecelist, this.getColor())) {
											piecelist[a][b].setCaptured(false);
											this.setCurrPosition(tempCurrPosition);
											return false;
										}
										else {
											if (this.getMoved() == false) {
												this.setMoved(true);
											}
											return true;
										}
									}
									else {
										return false;
									}
								}
							}
						}
					}
				}
			}
		}
		else if (xDiff == 0) {
			for (int a = 0; a < 2; a++) {
				for (int b = 0; b < 16; b++) {
					int blockerX = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
					int blockerY = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
					if (oldY < newY) {
						for (int c = oldY; c <= newY; c++) {
							if (blockerX == oldX && (blockerY>oldY && blockerY<newY)) {
								return false;
							}
							else if (blockerX == oldX && blockerY == newY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else if (piecelist[a][b].getID().equals("K")) {
									return false;
								}
								else {
									String tempCurrPosition = this.getCurrPosition();
									this.setCurrPosition(newPosition);
									piecelist[a][b].setCaptured(true);
									if (King.checkChecker(piecelist, this.getColor())) {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return false;
									}
									else {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return true;
									}
								}
							}
						}
					}
					else if (oldY > newY) {
						for (int c = newY; c <= oldY; c++) {
							if (blockerX == oldX && (blockerY<oldY && blockerY>newY)) {
								return false;
							}
							else if (blockerX == oldX && blockerY == newY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else if (piecelist[a][b].getID().equals("K")) {
									return false;
								}
								else {
									String tempCurrPosition = this.getCurrPosition();
									this.setCurrPosition(newPosition);
									piecelist[a][b].setCaptured(true);
									if (King.checkChecker(piecelist, this.getColor())) {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return false;
									}
									else {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		else if (yDiff == 0) {
			for (int a = 0; a < 2;a++) {
				for (int b=0;b<16;b++) {
					int blockerX = Chess.convertMove(piecelist[a][b].getCurrPosition())[0];
					int blockerY = Chess.convertMove(piecelist[a][b].getCurrPosition())[1];
					if (oldX < newX) {
						for (int c = oldX; c <= newX; c++) {
							if (blockerY == oldY && (blockerX>oldX && blockerX<newX)) {
								return false;
							}
							else if (blockerX == newX && blockerY == oldY) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else if (piecelist[a][b].getID().equals("K")) {
									return false;
								}
								else {
									String tempCurrPosition = this.getCurrPosition();
									this.setCurrPosition(newPosition);
									piecelist[a][b].setCaptured(true);
									if (King.checkChecker(piecelist, this.getColor())) {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return false;
									}
									else {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return true;
									}
								}
							}
						}
					}
					else if (oldX > newX) {
						for (int c = newX; c <= oldX; c++) {
							if (blockerY == oldY && (blockerX<oldX && blockerX>newX)) {
								return false;
							}
							else if (blockerY == oldY && blockerX == newX) {
								if (piecelist[a][b].getColor() == this.getColor()) {
									return false;
								}
								else if (piecelist[a][b].getID().equals("K")) {
									return false;
								}
								else {
									String tempCurrPosition = this.getCurrPosition();
									this.setCurrPosition(newPosition);
									piecelist[a][b].setCaptured(true);
									if (King.checkChecker(piecelist, this.getColor())) {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return false;
									}
									else {
										piecelist[a][b].setCaptured(false);
										this.setCurrPosition(tempCurrPosition);
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
	
	
	/** 
	 * @param moveTo
	 * @param piecelist
	 * @return Piece[][]
	 */
	public Piece[][] move(String moveTo, Piece[][] piecelist){
		int enemyColor;
		if (this.getColor() == false) {
			enemyColor = 1;
		}
		else {
			enemyColor = 0;
		}
		
		for (int i = 0; i < 16; i++) {
			if ((piecelist[enemyColor][i].getCurrPosition().equals(moveTo)) && (piecelist[enemyColor][i].getCaptured()==false)) {
				piecelist[enemyColor][i].setCaptured(true);
			}
		}
		if (this.getMoved() == false) {
			this.setMoved(true);
		}
		this.setPrevPosition(this.getCurrPosition());
		this.setCurrPosition(moveTo);
		return piecelist;
	}
}