package pieces;

import chess.Chess;
import java.lang.Math;

public class Rook extends Piece{
	
	public Rook(String id, boolean c, String p) {
		setID(id);
		setColor(c);
		setPieceType("R");
		setPrevPosition(p);
		setCurrPosition(p);
	}
	
	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return Piece[][]
	 */
	public Piece[][] move(String newPosition, Piece[][] piecelist){
		return null;
	}
	
	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return boolean
	 */
	public boolean validMove(String newPosition, Piece[][] piecelist){
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int oldX = currLocation[0];
		int oldY = currLocation[1];
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff = Math.abs(newX-oldX);
		int yDiff = Math.abs(newY-oldY);
		
		if ((xDiff != 0) && (yDiff != 0)) {
			return false;
		}
		else if ((xDiff != 0) || (yDiff != 0)){
			int capturing = 0;
			int captureX = 0;
			int captureY = 0;
			for (int z = 0; z < 2;z++) {
				for (int y = 0; y < 16; y++) {
					int blockerX = Chess.convertMove(piecelist[z][y].getCurrPosition())[0];
					int blockerY = Chess.convertMove(piecelist[z][y].getCurrPosition())[1];
					if (blockerY == newY && blockerX == newX) {
						if (piecelist[z][y].getColor() == this.getColor()) {
							return false;
						}
						else {
							capturing = 1;
							captureX = z;
							captureY = y;
							piecelist[z][y].setCaptured(true);
						}
					}
				}
			}
			String tempPrevPosition = this.getPrevPosition();
			this.setPrevPosition(this.getCurrPosition());
			this.setCurrPosition(newPosition);
			if (King.checkChecker(piecelist, this.getColor())) {
				this.setCurrPosition(this.getPrevPosition());
				this.setPrevPosition(tempPrevPosition);
				if (capturing == 1) {
					piecelist[captureX][captureY].setCaptured(false);
					capturing = 0;
				}
				return false;
			}
			else {
				this.setMoved(true);
				return true;
			}
		}
		return true;
	}
}