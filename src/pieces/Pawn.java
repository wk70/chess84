package pieces;

import chess.Chess;

public class Pawn extends Piece{
	/**
	 * Pawn constrcutor
	 * @param id ID of piece
	 * @param c Boolean controlling whether or not piece has been captured
	 * @param p Current position of piece (also previous position when constructed)
	 */
	public Pawn(String id, boolean c, String p) {
		setID(id);
		setColor(c);
		setPieceType("P");
		setPrevPosition(p);
		setCurrPosition(p);
		setMoved(false);
	}
	/**
	 * Method that handles movement of pawn, implementation of abstract Piece.move()
	 * @param newPosition
	 * @param piecelist
	 * @return updated piecelist of type Piece[][]
	 */
	public Piece[][] move(String newPosition, Piece[][] piecelist){
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int currX = currLocation[0];
		int currY = currLocation[1];
		
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff, yDiff;

		Piece selectedPiece = Chess.getPiece(this.getCurrPosition(), piecelist);
		Piece targetPiece = Chess.getPiece(newPosition, piecelist);
		
		int[] targetLocation = Chess.getPiecePosition(newPosition, piecelist);
		
		if(!this.getColor()){ // black
			
			xDiff = newX-currX;
			yDiff = newY-currY;
			if(currY == 1){
				this.setMoved(false);
				System.out.println("First movement Black");
			} else {
				this.setMoved(true);
			}
			System.out.println("B" + this.getMoved());
		} else { // white
			
			xDiff = currX-newX;
			yDiff = currY-newY;
			if(currY == 6){
				this.setMoved(false);
				System.out.println("First movement White");
			} else {
				this.setMoved(true);
			}
			System.out.println("W" + this.getMoved());
		}


		if(validMove(newPosition, piecelist)){ //move is valid
			
			if(Math.abs(xDiff) == 1 && yDiff == 1 && targetPiece == null)
		{
			try{
				int adjacentSpot1 = currX - 1;
				String adjacentSpot1Con = (Chess.letterConverter(adjacentSpot1) + "").trim();
				adjacentSpot1Con += currY;
	
				Piece adjacentSpot1P = Chess.getPiece(adjacentSpot1Con, piecelist);
				int[] adj1PrevPos = Chess.convertMove(adjacentSpot1P.getPrevPosition());
				int[] adj1CurrPos = Chess.convertMove(adjacentSpot1P.getCurrPosition());

				
				if(Math.abs(xDiff) == 1 && yDiff == 1 && (adjacentSpot1P.getClass() == Pawn.class) && 
				(adj1PrevPos[0] + 2 == adj1CurrPos[0]) && (adjacentSpot1P.getColor() != this.getColor())
				&& (adj1CurrPos[1] == currY)){
					piecelist[adj1CurrPos[0]][adj1CurrPos[1]].setCaptured(true); // Set target piece as captured
				
					this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
					this.setCurrPosition(newPosition); // set current position to desired move
					return piecelist;
		
				}
			} catch (Exception e){int adjacentSpot1 = -99;}
			try{
				int adjacentSpot2 = currX + 1;
				String adjacentSpot2Con = (Chess.letterConverter(adjacentSpot2) + "").trim();
				adjacentSpot2Con += currY;
	
				Piece adjacentSpot2P = Chess.getPiece(adjacentSpot2Con, piecelist);
				int[] adj2PrevPos = Chess.convertMove(adjacentSpot2P.getPrevPosition());
				int[] adj2CurrPos = Chess.convertMove(adjacentSpot2P.getCurrPosition());

				
				if(Math.abs(xDiff) == 1 && yDiff == 1 && (adjacentSpot2P.getClass() == Pawn.class) && 
				(adj2PrevPos[0] + 2 == adj2CurrPos[0]) && (adjacentSpot2P.getColor() != this.getColor())
				&& (adj2CurrPos[1] == currY)){
					piecelist[adj2CurrPos[0]][adj2CurrPos[1]].setCaptured(true); // Set target piece as captured
				
					this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
					this.setCurrPosition(newPosition); // set current position to desired move
					return piecelist;
				
				}
			} catch (Exception e){int adjacentSpot2 = -99;}
		
		}
			
			
			if(Math.abs(xDiff) == 1 && yDiff == 1 && (selectedPiece.getColor() != targetPiece.getColor()) && !targetPiece.getCaptured()){ // caputring move, check for correct movement and if 
				
				piecelist[targetLocation[0]][targetLocation[1]].setCaptured(true); // Set target piece as captured
				
				this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
				this.setCurrPosition(newPosition); // set current position to desired move
				System.out.println(this.getCurrPosition() + "   " + newPosition);

				return piecelist;
			}
			if((xDiff == 0 && yDiff == 1) || (xDiff == 0 && yDiff == 2 && !getMoved())){ // normal movement
				this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
				this.setCurrPosition(newPosition); // set current position to desired move
				System.out.println(this.getCurrPosition() + "   " + newPosition);
				
				return piecelist;
			}
			
			try{
				int adjacentSpot1 = currX - 1;
				String adjacentSpot1Con = (Chess.letterConverter(adjacentSpot1) + "").trim();
				adjacentSpot1Con += currY;

				Piece adjacentSpot1P = Chess.getPiece(adjacentSpot1Con, piecelist);
				int[] adj1PrevPos = Chess.convertMove(adjacentSpot1P.getPrevPosition());
				int[] adj1CurrPos = Chess.convertMove(adjacentSpot1P.getCurrPosition());
	
				
				if(Math.abs(xDiff) == 1 && yDiff == 1 && (adjacentSpot1P.getClass() == Pawn.class) && 
				(adj1PrevPos[0] == adj1CurrPos[0])
				&& (Chess.convertMove(adjacentSpot1P.getPrevPosition())[1] == currY)){
					piecelist[targetLocation[0]][targetLocation[1]].setCaptured(true); // Set target piece as captured
				
				this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
				this.setCurrPosition(newPosition); // set current position to desired move
				System.out.println(this.getCurrPosition() + "   " + newPosition);
				}
			} catch (Exception e){int adjacentSpot1 = -99;}
			try{
				int adjacentSpot2 = currX + 1;
				String adjacentSpot2Con = (Chess.letterConverter(adjacentSpot2) + "").trim();
				adjacentSpot2Con += currY;

				Piece adjacentSpot2P = Chess.getPiece(adjacentSpot2Con, piecelist);
				int[] adj2PrevPos = Chess.convertMove(adjacentSpot2P.getPrevPosition());
				int[] adj2CurrPos = Chess.convertMove(adjacentSpot2P.getCurrPosition());
				
				if((Math.abs(xDiff) == 1 && yDiff == 1) && (adjacentSpot2P.getClass() == Pawn.class) &&
				(adj2PrevPos[0] == adj2CurrPos[0])
				&& ((Chess.convertMove(adjacentSpot2P.getPrevPosition())[1] == currY))){
					
					this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
					this.setCurrPosition(newPosition); // set current position to desired move
					System.out.println(this.getCurrPosition() + "   " + newPosition);
				}
			} catch (Exception e){int adjacentSpot2 = -99;}
		}

		return piecelist;
	}

	/**
	 * Overloaded move method for pawn that handles movement and promotion
	 * @param newPosition Desired position
	 * @param piecelist Piecelist containing all game pieces
	 * @param promotion Intended promotional target
	 * @return updated piecelist of type Piece[][]
	 */
	public Piece[][] move(String newPosition, Piece[][] piecelist, String promPiece){
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int currX = currLocation[0];
		int currY = currLocation[1];
		
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff, yDiff;

		Piece selectedPiece = Chess.getPiece(this.getCurrPosition(), piecelist);
		Piece targetPiece = Chess.getPiece(newPosition, piecelist);

		int[] pieceListLocation = Chess.getPiecePosition(this.getCurrPosition(), piecelist);
		boolean canPromote = false;
		
		if(!this.getColor()){ // black
			
			xDiff = newX-currX;
			yDiff = newY-currY;
			if(currY == 1){
				this.setMoved(false);
				System.out.println("First movement Black");
			} else {
				this.setMoved(true);
			}
			if(currY == 7){
				canPromote = true;
			} else {
				canPromote = false;
			}
			System.out.println("B" + this.getMoved());
		} else { // white
			
			xDiff = currX-newX;
			yDiff = currY-newY;
			if(currY == 6){
				this.setMoved(false);
				System.out.println("First movement White");
			} else {
				this.setMoved(true);
			}
			if(newY == 0){
				canPromote = true;
			} else {
				canPromote = false;
			}
			System.out.println("W" + this.getMoved());
		}


		if(validMove(newPosition, piecelist)){ //move is valid
			if((Math.abs(xDiff) == 1 && yDiff == 1 && (selectedPiece.getColor() != targetPiece.getColor()) && !targetPiece.getCaptured())){ // caputring move, check for correct movement and if 
				int[] targetLocation = Chess.getPiecePosition(newPosition, piecelist);
				piecelist[targetLocation[0]][targetLocation[1]].setCaptured(true); // Set target piece as captured
				System.out.println(piecelist[targetLocation[0]][targetLocation[1]]);
				
				
					selectedPiece = piecelist[pieceListLocation[0]][pieceListLocation[1]];
				this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
				this.setCurrPosition(newPosition); // set current position to desired move

				if(canPromote)
					piecelist = promotion(selectedPiece, promPiece, piecelist);
				

				return piecelist;
			}
			if((xDiff == 0 && yDiff == 1) || (xDiff == 0 && yDiff == 2 && !getMoved()) && (targetPiece == null || targetPiece.getCaptured())){ // normal movement
				
				
				selectedPiece = piecelist[pieceListLocation[0]][pieceListLocation[1]];
				this.setPrevPosition(this.getCurrPosition()); // new previous position is now current position
				this.setCurrPosition(newPosition); // set current position to desired move
				
				if(canPromote)
					piecelist = promotion(selectedPiece, promPiece, piecelist);
				

				return piecelist;
			}
			
			
		}

		return piecelist;
	}

	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return boolean
	 */
	public boolean validMove(String newPosition, Piece[][] piecelist) {
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int currX = currLocation[0];
		int currY = currLocation[1];
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff, yDiff;


		Piece selectedPiece = Chess.getPiece(this.getCurrPosition(), piecelist);
		Piece targetPiece = Chess.getPiece(newPosition, piecelist);

		xDiff = newX-currX;
		yDiff = newY-currY;

		xDiff = Math.abs(xDiff);
		yDiff = Math.abs(yDiff);
		
		System.out.print("currX: " + currX);
		System.out.println(" currY: " + currY);

		if(!this.getColor()){ // black
			
			
			if(currY == 1){
				this.setMoved(false);
			} else {
				this.setMoved(true);
			}
			System.out.println("B" + this.getMoved());
		} else { // white
			
			if(currY == 6){
				this.setMoved(false);
			} else {
				this.setMoved(true);
			}
			System.out.println("W" + this.getMoved());
		}

		
		System.out.println("xDiff: " + xDiff + " yDiff: " + yDiff); // && targetPiece == null || targetPiece
		
		
		if(xDiff > 1){ // checks if pawn is trying to move horizontally by more than 1, which is not possible
			return false;
		} else if(Math.abs(xDiff) == 1 && yDiff == 1 && targetPiece == null)
		{
			try{
				int adjacentSpot1 = currX - 1;
				String adjacentSpot1Con = (Chess.letterConverter(adjacentSpot1) + "").trim();
				adjacentSpot1Con += Math.abs(currY - 8);
	
				Piece adjacentSpot1P = Chess.getPiece(adjacentSpot1Con, piecelist);
				int[] adj1PrevPos = Chess.convertMove(adjacentSpot1P.getPrevPosition());
				int[] adj1CurrPos = Chess.convertMove(adjacentSpot1P.getCurrPosition());

				
				if(Math.abs(xDiff) == 1 && yDiff == 1 && (adjacentSpot1P.getClass() == Pawn.class) && 
				(adj1PrevPos[0] + 2 == adj1CurrPos[0]) && (adjacentSpot1P.getColor() != this.getColor())
				&& (adj1CurrPos[1] == Math.abs(currY - 8))){
					return true;
				}
			} catch (Exception e){int adjacentSpot1 = -99;}
			try{
				int adjacentSpot2 = currX + 1;
				String adjacentSpot2Con = (Chess.letterConverter(adjacentSpot2) + "").trim();
				adjacentSpot2Con += Math.abs(currY - 8);
	
				Piece adjacentSpot2P = Chess.getPiece(adjacentSpot2Con, piecelist);
				int[] adj2PrevPos = Chess.convertMove(adjacentSpot2P.getPrevPosition());
				int[] adj2CurrPos = Chess.convertMove(adjacentSpot2P.getCurrPosition());

				
				if(Math.abs(xDiff) == 1 && yDiff == 1 && (adjacentSpot2P.getClass() == Pawn.class) && 
				(adj2PrevPos[0] + 2 == adj2CurrPos[0]) && (adjacentSpot2P.getColor() != this.getColor())
				&& (adj2CurrPos[1] == Math.abs(currY - 8))){
					return true;
				}
			} catch (Exception e){int adjacentSpot2 = -99;}
		
		} else if(Math.abs(xDiff) == 1 && yDiff == 1 && (selectedPiece.getColor() != targetPiece.getColor()) && !targetPiece.getCaptured()){ // case where pawn is moving horizontally by 1, in which case it also has to move vertically by 1
			// if the piece in the direct diagonal is the opposite color
			return true;
			
		}
		 // if it's the pawns first move, it can move 2 spaces
		if(yDiff == 2 && getMoved() == false && (targetPiece == null || targetPiece.getCaptured())){ // if pawn is trying to move vertically by more than 1, only possible if it is the first move
			return true;
		}
		if(yDiff == 1 && (targetPiece == null || targetPiece.getCaptured())) // standard move, this is valid
			return true;

		
			



		
		return false; // if the pawn doesn't meet any of these cases, move is invalid
		

		

	}
	
	
	/** 
	 * @param p
	 * @param newPiece
	 * @param plist
	 * @return Piece[][]
	 */
	public Piece[][] promotion(Piece p, String newPiece, Piece[][] plist) {
		int[] pLL = Chess.getPiecePosition(p.getCurrPosition(), plist);
		
		if (p.getColor()) {
			if (newPiece.equals("N")) {
				this.setCaptured(true);
				Knight pK = new Knight("wN",true,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pK;
				return plist;
			}
			else if (newPiece.equals("B")) {
				this.setCaptured(true);
				Bishop pB = new Bishop("wB",true,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pB;
				return plist;
			}
			else if (newPiece.equals("R")) {
				this.setCaptured(true);
				Rook pR = new Rook("wR",true,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pR;
				return plist;
			}
			else {
				this.setCaptured(true);
				Queen pQ = new Queen("wQ",true,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pQ;
				return plist;
			}
		}
		else if (!p.getColor()) {
			if (newPiece.equals("N")) {
				this.setCaptured(true);
				Knight pK = new Knight("bN",false,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pK;
				return plist;
			}
			else if (newPiece.equals("B")) {
				this.setCaptured(true);
				Bishop pB = new Bishop("bB",false,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pB;
				return plist;
			}
			else if (newPiece.equals("R")) {
				this.setCaptured(true);
				Rook pR = new Rook("bR",false,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pR;
				return plist;
			}
			else {
				this.setCaptured(true);
				Queen pQ = new Queen("bQ",false,this.getCurrPosition());
				plist[pLL[0]][pLL[1]] = pQ;
				return plist;
			}
		}
		else {
			return plist;
		}
	}
}