package pieces;

import chess.Chess;
import java.lang.Math;

public class Knight extends Piece{
	
	public Knight(String id, boolean c, String p) {
		setID(id);
		setColor(c);
		setPieceType("N");
		setPrevPosition(p);
		setCurrPosition(p);
	}
	
	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return Piece[][]
	 */
	public Piece[][] move(String newPosition, Piece[][] piecelist){
		return null;
	}
	
	
	/** 
	 * @param newPosition
	 * @param piecelist
	 * @return boolean
	 */
	public boolean validMove(String newPosition, Piece[][] piecelist) {
		int[] newLocation = Chess.convertMove(newPosition);
		int[] currLocation = Chess.convertMove(this.getCurrPosition());
		int oldX = currLocation[0];
		int oldY = currLocation[1];
		int newX = newLocation[0];
		int newY = newLocation[1];
		int xDiff = Math.abs(newX-oldX);
		int yDiff = Math.abs(newY-oldY);
		System.out.println("Old: " +oldX + " " + oldY);
		System.out.println("New: " + newX + " " + newY);
		
		if ((xDiff == 2 && yDiff == 1) || (yDiff == 2 && xDiff == 1)) {
			int capturing = 0;
			int captureX = 0;
			int captureY = 0;
			String tempPrevPosition = this.getPrevPosition();
			this.setPrevPosition(this.getCurrPosition());
			for (int x = 0; x<2;x++) {
				for(int y = 0; y<16;y++) {
					if (piecelist[x][y].getCurrPosition().equals(newPosition)) {
						if (piecelist[x][y].getColor() == this.getColor()) {
							this.setCurrPosition(this.getPrevPosition());
							this.setPrevPosition(tempPrevPosition);
							System.out.println("a");
							return false;
						}
						else {
							capturing = 1;
							captureX = x;
							captureY = y;
							piecelist[x][y].setCaptured(true);
						}
					}
					this.setCurrPosition(newPosition);
					if (King.checkChecker(piecelist, this.getColor())) {
						this.setCurrPosition(this.getPrevPosition());
						this.setPrevPosition(tempPrevPosition);
						if (capturing == 1) {
							piecelist[captureX][captureY].setCaptured(false);
							capturing = 0;
						}
						System.out.println("b");
						return false;
					}
					else {
						if (this.getMoved() == false) {
							this.setMoved(true);
						}
						System.out.println("c");
						return true;
					}
				}
			}
		}
		return true;
	}
}