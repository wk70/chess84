package chess;

import pieces.Piece;
import pieces.Rook;
import pieces.Knight;
import pieces.Bishop;
import pieces.Queen;
import pieces.King;
import pieces.Pawn;
import java.util.Scanner;

public class Chess{
	
	/**
	 * 
	 * @param position String representing board position, ie A1
	 * @return returns integer array of 2 digits representing position, [column, row]
	 */
	
	 /**
	  * Method that returns piece from its position on the board (sourced from the game's piecelist)
	  * @param color Color of piece to search for, 0 = black, 1 = white
	  * @param position current position of piece, i.e. a1
	  * @param piecelist piecelist to find piece in
	  * @return null if piece is not found, or piece at position 
	  */
	public static Piece getPiece(String position, Piece[][] piecelist)
	{
		Piece selectedPiece = null;
		for(int j = 0; j < 2; j++){
			for (int q = 0; q < 16; q++) { // find piece on board given origin position
				if (piecelist[j][q].getCurrPosition().equals(position)) {
					selectedPiece = piecelist[j][q];
				}
			}
		}
		return selectedPiece;
	}
	/**
	 * Method that returns Piece location on piecelist given board location
	 * @param position Location of target piece on the board
	 * @param piecelist Target piecelist
	 * @return returns 2 dimension int array with position of piece in piecelist
	 */
	public static int[] getPiecePosition(String position, Piece[][] piecelist)
	{
		int[] arrayPos = new int[2];
		for(int j = 0; j < 2; j++){
			for (int q = 0; q < 16; q++) { // find piece on board given origin position
				if (piecelist[j][q].getCurrPosition().equals(position)) {
					arrayPos[0] = j;
					arrayPos[1] = q;
				}
			}
		}
		return arrayPos;
	}



	/**
	 * Method that converts from chess position to ints that correspond to location on chess board
	 * @param position Current position, i.e a1
	 * @return returns integer array of length 2, [column, row]
	 */
	public static int[] convertMove(String position){
		int col = 0;
		int[] location = new int[2];
		char column = position.charAt(0);
		int row = 8-Character.getNumericValue(position.charAt(1));
		if (column == 'a') {
			col = 0;
		}
		else if (column == 'b') {
			col = 1;
		}
		else if (column == 'c') {
			col = 2;
		}
		else if (column == 'd') {
			col = 3;
		}
		else if (column == 'e') {
			col = 4;
		}
		else if (column == 'f') {
			col = 5;
		}
		else if (column == 'g') {
			col = 6;
		}
		else if (column == 'h') {
			col = 7;
		}
		location[1] = row;
		location[0] = (int) col;
		return location;
	}
	/**
	 * Converts int position from chess board to corresponding letter position
	 * @param x int position on board (column #)
	 * @return returns corresponding column letter
	 */
	public static char letterConverter(int x) {
		char c = 'z';
		if (x == 0) {
			return 'a';
		}
		else if (x == 1) {
			return 'b';
		}
		else if (x == 2) {
			return 'c';
		}
		else if (x == 3) {
			return 'd';
		}
		else if (x == 4) {
			return 'e';
		}
		else if (x == 5) {
			return 'f';
		}
		else if (x == 6) {
			return 'g';
		}
		else if (x == 7) {
			return 'h';
		}
		return c;
	}
	/**
	 * Initializes set of game pieces for a new chess game
	 * @return full set of chess pieces
	 */
	public static Piece[][] pieceCreator() {
		Piece[][] pieceList = new Piece[2][16];
		Rook BLR = new Rook("bR",false,"a8");
		Rook BRR = new Rook("bR",false,"h8");
		Knight BLN = new Knight("bN",false,"b8");
		Knight BRN = new Knight("bN",false,"g8");
		Bishop BLB = new Bishop("bB",false,"c8");
		Bishop BRB = new Bishop("bB",false,"f8");
		Queen BQ = new Queen("bQ",false,"d8");
		King BK = new King("bK",false,"e8");
		Pawn BP1 = new Pawn("bp",false,"a7");
		Pawn BP2 = new Pawn("bp",false,"b7");
		Pawn BP3 = new Pawn("bp",false,"c7");
		Pawn BP4 = new Pawn("bp",false,"d7");
		Pawn BP5 = new Pawn("bp",false,"e7");
		Pawn BP6 = new Pawn("bp",false,"f7");
		Pawn BP7 = new Pawn("bp",false,"g7");
		Pawn BP8 = new Pawn("bp",false,"h7");

		Rook WLR = new Rook("wR",true,"a1");
		Rook WRR = new Rook("wR",true,"h1");
		Knight WLN = new Knight("wN",true,"b1");
		Knight WRN = new Knight("wN",true,"g1");
		Bishop WLB = new Bishop("wB",true,"c1");
		Bishop WRB = new Bishop("wB",true,"f1");
		Queen WQ = new Queen("wQ",true,"d1");
		King WK = new King("wK",true,"e1");
		Pawn WP1 = new Pawn("wp",true,"a2");
		Pawn WP2 = new Pawn("wp",true,"b2");
		Pawn WP3 = new Pawn("wp",true,"c2");
		Pawn WP4 = new Pawn("wp",true,"d2");
		Pawn WP5 = new Pawn("wp",true,"e2");
		Pawn WP6 = new Pawn("wp",true,"f2");
		Pawn WP7 = new Pawn("wp",true,"g2");
		Pawn WP8 = new Pawn("wp",true,"h2");
		
		pieceList[0][0] = BLR;
		pieceList[0][1] = BRR;
		pieceList[0][2] = BLN;
		pieceList[0][3] = BRN;
		pieceList[0][4] = BLB;
		pieceList[0][5] = BRB;
		pieceList[0][6] = BQ;
		pieceList[0][7] = BK;
		pieceList[0][8] = BP1;
		pieceList[0][9] = BP2;
		pieceList[0][10] = BP3;
		pieceList[0][11] = BP4;
		pieceList[0][12] = BP5;
		pieceList[0][13] = BP6;
		pieceList[0][14] = BP7;
		pieceList[0][15] = BP8;
		
		
		pieceList[1][0] = WLR;
		pieceList[1][1] = WRR;
		pieceList[1][2] = WLN;
		pieceList[1][3] = WRN;
		pieceList[1][4] = WLB;
		pieceList[1][5] = WRB;
		pieceList[1][6] = WQ;
		pieceList[1][7] = WK;
		pieceList[1][8] = WP1;
		pieceList[1][9] = WP2;
		pieceList[1][10] = WP3;
		pieceList[1][11] = WP4;
		pieceList[1][12] = WP5;
		pieceList[1][13] = WP6;
		pieceList[1][14] = WP7;
		pieceList[1][15] = WP8;
		
		return pieceList;
	}
	
	/**
	 * Initializes board from piecelist
	 * @param pieceList piecelist to use on board
	 * @return returns generated board as String[][]
	 */

	public static String[][] boardMaker(Piece[][] pieceList){
		String[][] board = new String[9][9];
		for (int a = 0; a < 9;a++) {
			for (int b = 0; b < 9; b++) {
				if (a == 8) {
					switch(b) {
						case 0: 
						  board[a][b] = " a";
						  break;
						case 1:
						  board[a][b] = " b";
						  break;
						case 2:
						  board[a][b] = " c";
						  break;
						case 3:
						  board[a][b] = " d";
						  break;
						case 4:
						  board[a][b] = " e";
						  break;
						case 5:
						  board[a][b] = " f";
						  break;
						case 6:
						  board[a][b] = " g";
						  break;
						case 7:
						  board[a][b] = " h";
						  break;
						case 8:
						  board[a][b] = "  ";
						  break;
					}
				}
				else if (b == 8) {
					
					switch(a) {
					  case 0:
						board[a][b] = "8";
						break;
					  case 1:
						board[a][b] = "7";
						break;
					  case 2:
						board[a][b] = "6";
						break;
					  case 3:
						board[a][b] = "5";
						break;
					  case 4:
						board[a][b] = "4";
						break;
					  case 5:
						board[a][b] = "3";
						break;
					  case 6:
						board[a][b] = "2";
						break;
					  case 7:
						board[a][b] = "1";
						break;
					}
				}
				else if ((a % 2) == 0) {
					if ((b % 2) == 0) {
						board[a][b] = "  ";
					}
					else {
						board[a][b] = "##";
					}
				}
				else {
					if ((b % 2) == 0) {
						board[a][b] = "##";
					}
					else {
						board[a][b] = "  ";
					}
				}
			}
		}
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 16; j++) {
				int[] locale = convertMove(pieceList[i][j].getCurrPosition());
				int xd = locale[1];
				int yc = locale[0];
				if (pieceList[i][j].getCaptured() == false) {
					board[xd][yc] = pieceList[i][j].getID();
				}
			}
		}
		return board;		
	}
	
	/**
	 * Prints generated board
	 * @param board Board to be printed, in String[][] format
	 */
	public static void boardPrinter(String[][] board) {
		System.out.println();
		String row = "";
		for (int c = 0; c<9;c++) {
			for (int d = 0; d<9;d++) {
				if (d != 8) {
					row = row + board[c][d] + " ";
				}
				else {
					row = row + board[c][d];
				}
			}
			System.out.println(row);
			row = "";
		}
		System.out.println();
	}
	
	/**
	 * Method that handles game logic
	 * @param pieceList piecelist to use in game
	 */
	public static void game(Piece[][] pieceList) {
		Scanner sc = new Scanner(System.in);
		
		int gameOn = 1;
		int moveCounter = 0;
		int blackTurn = 0;
		int whiteTurn = 1;
		String moveTo;
		String moveFrom;
		Piece selectedPiece = null;

		while (gameOn == 1) {
		
			if ((moveCounter % 2) == 0) {
				whiteTurn = 1;
				blackTurn = 0;
			}
			else {
				blackTurn = 1;
				whiteTurn = 0;
			}
			
			while (whiteTurn == 1) {
			  try{
				int promotion = 0;
				String[][] board = boardMaker(pieceList);
				boardPrinter(board);
				//Scanner n = new Scanner(System.in);
				System.out.println("White's Move: ");
				String wMove = sc.nextLine();

				moveFrom = wMove.substring(0,2);
				moveTo = wMove.substring(3,5);

				System.out.println(moveTo + "\n" + moveFrom);

				int[] currLocation = Chess.convertMove(moveFrom);
				int currX = currLocation[0];
				int currY = currLocation[1];

				int[] newLocation = Chess.convertMove(moveTo);
				int newX = newLocation[0];
				int newY = newLocation[1];

				selectedPiece = getPiece(moveFrom, pieceList);

				if(selectedPiece == null || selectedPiece.getColor() == false){ // if no valid piece could be found, or a black piece was selected
					System.out.println("Invalid selection, please try again.");
					continue;
				}
				
				if (wMove.equals("resign")) {
					System.out.println("Black wins");
					whiteTurn = 0;
					gameOn = 0;
				}
				else if ((wMove.length() == 11)&&(wMove.substring(wMove.length()-5).equals("draw?"))) {
					System.out.println("draw");
					whiteTurn = 0;
					gameOn = 0;
				}
				else if(selectedPiece.validMove(moveTo, pieceList)){
					
					String promoPiece = null;
					if (newY == 0 && selectedPiece.getClass() == Pawn.class) {
						promotion = 1;
						promoPiece = wMove.substring(5).trim();
						pieceList = ((Pawn) selectedPiece).move(moveTo, pieceList, promoPiece);
					} else {
						pieceList = selectedPiece.move(moveTo, pieceList);
					}
					
					moveCounter++;
					blackTurn = 1;
					whiteTurn = 0;
				
				} else {
					System.out.println("Invalid move, please try again");
				}
		
			} catch (Exception e){
				System.out.println("Invalid selection, please try again.");
			}
		}
			while (blackTurn == 1) {
				try{
					int promotion = 0;
					String[][] board = boardMaker(pieceList);
					boardPrinter(board);
					//Scanner n = new Scanner(System.in);
					System.out.println("Black's Move: ");
					String wMove = sc.nextLine();
	
					moveFrom = wMove.substring(0,2);
					moveTo = wMove.substring(3,5);
	
					System.out.println(moveTo + "\n" + moveFrom);
	
					int[] currLocation = Chess.convertMove(moveFrom);
					int currX = currLocation[0];
					int currY = currLocation[1];
	
					int[] newLocation = Chess.convertMove(moveTo);
					int newX = newLocation[0];
					int newY = newLocation[1];
	
					selectedPiece = getPiece(moveFrom, pieceList);
	
					if(selectedPiece == null || selectedPiece.getColor() == true){ // if no valid piece could be found, or a black piece was selected
						System.out.println("Invalid selection, please try again.");
						continue;
					}
					
					if (wMove.equals("resign")) {
						System.out.println("White wins");
						whiteTurn = 0;
						gameOn = 0;
					}
					else if ((wMove.length() == 11)&&(wMove.substring(wMove.length()-5).equals("draw?"))) {
						System.out.println("draw");
						whiteTurn = 0;
						gameOn = 0;
					}
					else if(selectedPiece.validMove(moveTo, pieceList)){
						
						String promoPiece = null;
						if (newY == 0 && selectedPiece.getClass() == Pawn.class) {
							promotion = 1;
							promoPiece = wMove.substring(5).trim();
							pieceList = ((Pawn) selectedPiece).move(moveTo, pieceList, promoPiece);
						} else {
							pieceList = selectedPiece.move(moveTo, pieceList);
						}
						for(int q = 0; q < 16; q++){
						pieceList[0][q].move(moveTo, pieceList);
								if (King.checkChecker(pieceList, !pieceList[0][q].getColor())) {
									int checkmateChecker = 0;
									int wkX = convertMove(pieceList[0][7].getCurrPosition())[0];
									int wkY = convertMove(pieceList[0][7].getCurrPosition())[1];
									String tempCurrPosition = pieceList[0][7].getCurrPosition();
									for (int h = -1; h < 2; h++) {
										for (int o = -1; o < 2; o++) {
											String sb = "";
											char newXX = letterConverter(wkX+h);
											char newYY =  (char) (wkY+o);
											sb = sb+newXX+newYY;
											int tempTakenPieces = 0;
											for (int v = 0; v < 2;v++) {
												for (int u = 0; u < 16; u++) {
													if ((pieceList[v][u].getCurrPosition().equals(sb)) && pieceList[v][u].getColor()) {
														tempTakenPieces++;
													}
												}
											}
											if (pieceList[0][7].validMove(sb,pieceList)) {
												checkmateChecker = checkmateChecker+1;
											}
											while (tempTakenPieces != 0) {
												for (int v = 0; v < 2;v++) {
													for (int u = 0; u < 16; u++) {
														if ((pieceList[v][u].getCurrPosition().equals(sb)) && pieceList[v][u].getColor()) {
															pieceList[v][u].setCaptured(false);
															tempTakenPieces--;
														}
													}
												}
											}
										}
									}
									pieceList[0][7].setCurrPosition(tempCurrPosition);
									if (checkmateChecker == 0) {
										System.out.println("Checkmate");
										System.out.println("White");
									}
									else {
										System.out.println("Check");
									}
									checkmateChecker = 0;
								}
							}
						moveCounter++;
						blackTurn = 0;
						whiteTurn = 1;
					
					} else {
						System.out.println("Invalid move, please try again");
					}
			
				} catch (Exception e){
					System.out.println("Invalid selection, please try again.");
				}
				}
			//n.close();
			}
			}
		
	
	
	/** 
	 * @param args
	 */
	public static void main(String[] args) {
		Piece[][] pieceList = pieceCreator();
		
		game(pieceList);
		
	}
}